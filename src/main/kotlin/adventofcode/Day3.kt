package adventofcode

import java.io.File

class Day3(private val patchesStrings: List<String> = File("src/main/resources/day3.txt").readLines()) {

    fun assignment1(): Int {
        val patches = patchesStrings.map { getPatch(it) }

        return patches
                .mapIndexed { index, patch -> numberOfOverlaps(patch, patches.drop(index + 1)) }
                .flatten()
                .distinct()
                .count()
    }

    fun assignment2(): Int {
        val patches = patchesStrings.map { getPatch(it) }

        return sequenceOf(*patches.toTypedArray())
                .map { it to numberOfOverlaps(it, patches.minusElement(it)) }
                .first { it.second.isEmpty() }
                .first.elf
    }

    private fun numberOfOverlaps(currentPatch: Patch, otherPatches: List<Patch>): List<XY> = otherPatches
            .map { patch -> patch.overlaps(currentPatch) }
            .flatten()

    private fun getPatch(patchString: String): Patch {
        return patchString
                .split(Regex("[#|(\\h@\\h)|,|:\\h|x]"))
                .filter { it.isNotBlank() }
                .map { it.toInt() }
                .let { Patch(it[0], it[1], it[2], it[3], it[4]) }
    }

    data class Patch(val elf: Int, val x: Int, val y: Int, val xSize: Int, val ySize: Int) {
        private val xRange = generateSequence(x + 1) { it + 1 }.take(xSize).toList()
        private val yRange = generateSequence(y + 1) { it + 1 }.take(ySize).toList()
        private val points: List<XY> = xRange.map { x -> yRange.map { y -> x to y } }.flatten().map { XY.fromPair(it) }

        fun overlaps(other: Patch): Set<XY> {
            return points.intersect(other.points)
        }
    }
}