package adventofcode

import java.io.File
import java.time.LocalDateTime
import java.time.LocalTime

class Day4(private val stringMessages: List<String> = File("src/main/resources/day4.txt").readLines()) {
    fun assignment1(): Int {
        return getGuardSchedules()
                .maxBy { it.asleep.size }!!
                .let { guardSchedule -> guardSchedule.guard to guardSchedule.asleep.groupBy { it.toLocalTime() } }
                .let { pair -> pair.first to pair.second.maxBy { it.value.size }!!.key.minute }
                .let { it.first * it.second }
    }

    fun assignment2(): Int {
        return getGuardSchedules()
                .pmap { SleepyGuard.fromGuardSchedule(it) }
                .filterNot { it == null }
                .maxBy { it!!.numberOfSleeps }!!
                .let { it.guard * it.asleep.minute }
    }

    private fun getGuardSchedules(): List<GuardSchedule> {
        return stringMessages.map { parse(it) }.map { it.toMessage() }.sortedBy { it.dateTime }
                .dropWhile { it.guard == null }
                .fold(emptyList<Guard>()) { acc, message -> if (message.guard == null) acc + message.copy(guard = acc.last().guard!!) else acc + message }
                .groupBy { it.guard!! }
                .mapValues { entry -> entry.value.fold(emptyList<Guard>()) { acc, guard -> createGuardSchedule(acc, guard) } }
                .mapValues { entry -> entry.value.filter { isBetweenMidnightAnd1AM(it.dateTime) } }
                .map { entry ->
                    entry.value.fold(GuardSchedule(entry.key, emptyList(), emptyList())) { acc, guard ->
                        when (guard.state) {
                            State.ASLEEP -> acc.copy(asleep = acc.asleep + guard.dateTime)
                            else -> acc.copy(awake = acc.awake + guard.dateTime)
                        }
                    }
                }
    }

    private fun createGuardSchedule(acc: List<Guard>, guard: Guard) =
            when {
                acc.isEmpty() -> acc + guard
                else -> acc.plus(generateStateForAllMinutes(acc, guard)) + guard
            }

    private fun generateStateForAllMinutes(acc: List<Guard>, guard: Guard): Sequence<Guard> = generateSequence(acc.last().dateTime.plusMinutes(1)) { it.plusMinutes(1) }
            .takeWhile { it.isBefore(guard.dateTime) }
            .map { acc.last().copy(dateTime = it) }

    private fun parse(s: String): ParsingMessage {
        val line = s.split(Regex("[\\[(\\])]")).filterNot { it == "" }.map { it.trim() }
        return ParsingMessage(LocalDateTime.parse(line[0].replace(' ', 'T')), line[1])
    }

    private fun isBetweenMidnightAnd1AM(dateTime: LocalDateTime) =
            (dateTime.toLocalTime().isAfter(LocalTime.MIDNIGHT) || dateTime.toLocalTime() == LocalTime.MIDNIGHT) &&
                    dateTime.toLocalTime().isBefore(LocalTime.MIDNIGHT.plusHours(1).minusSeconds(1))

    data class SleepyGuard(val guard: Int, val asleep: LocalTime, val numberOfSleeps: Int) {
        companion object {
            fun fromGuardSchedule(guardSchedule: GuardSchedule): SleepyGuard? {
                val asleep = guardSchedule.asleep.map { it.toLocalTime() }.groupBy { it }.mapValues { it.value.size }.maxBy { it.value }
                return if (asleep != null) SleepyGuard(guardSchedule.guard, asleep.key, asleep.value) else null
            }
        }
    }

    data class GuardSchedule(val guard: Int, val awake: List<LocalDateTime>, val asleep: List<LocalDateTime>)

    data class Guard(val dateTime: LocalDateTime, val state: State, val guard: Int? = null)

    data class ParsingMessage(val dateTime: LocalDateTime, val messageString: String) {
        fun toMessage() = if (messageString.startsWith("Guard #")) {
            val split = messageString.split("Guard #", " ").filterNot { it == "" }
            Guard(dateTime, State.parse(split.drop(1).joinToString(" ")), split[0].toInt())
        } else Guard(dateTime, State.parse(messageString))
    }

    enum class State {
        STARTING,
        ASLEEP,
        AWAKE;

        companion object {

            fun parse(message: String): State {
                return when (message) {
                    "falls asleep" -> ASLEEP
                    "begins shift" -> STARTING
                    else -> AWAKE
            }
        }
    }
}
}
