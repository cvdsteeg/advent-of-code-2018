package adventofcode

import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlin.math.abs

data class XY(val x: Int, val y: Int) {
    fun calculateManhattanDistance(that: XY): Int {
        return abs(this.x - that.x) + abs(this.y - that.y)
    }

    operator fun plus(that: XY): XY {
        return XY(this.x + that.x, this.y + that.y)
    }

    operator fun plus(that: Int): XY {
        return XY(this.x + that, this.y + that)
    }

    operator fun minus(that: Int): XY {
        return XY(this.x - that, this.y - that)
    }

    companion object {
        val comparator = compareBy<XY>({ it.y }, { it.x })
        fun fromPair(it: Pair<Int, Int>) = XY(it.first, it.second)
    }
}

fun <A, B> Iterable<A>.pmap(f: suspend (A) -> B): List<B> = runBlocking {
    map { async { f(it) } }.map { it.await() }
}

fun <K, V, R> Map<out K, V>.pmap(f: suspend (Map.Entry<K, V>) -> R): List<R> = runBlocking {
    map { async { f(it) } }.map { it.await() }
}

fun generateGrid(min: XY, max: XY): List<XY> {
    return (min.x..max.x).map { x -> (min.y..max.y).map { y -> x to y } }.flatten().map { XY.fromPair(it) }.sortedWith(XY.comparator)
}

fun <A, B, C> ((B) -> C).compose(g: (A) -> B): (A) -> C = { x -> this(g(x)) }

fun <A, B, C> ((A) -> B).andThen(g: (B) -> C): (A) -> C = { x -> g(this(x)) }

fun <A, B, C> ((A, B) -> C).curried(): (A) -> (B) -> C = { a -> { b -> this(a, b) } }

fun <K, V, R> Collection<Pair<K, V>>.mapReverse(f: (V) -> R): Map<R, List<K>> =
        this.fold(emptyMap()) { acc, pair -> acc + (f(pair.second).let { it to (acc.getOrDefault(it, emptyList()) + pair.first) }) }

fun <K, V> Collection<Pair<K, V>>.toMapToList(): Map<K, List<V>> =
        this.fold(emptyMap()) { acc, pair -> acc + (pair.first.let { it to acc.getOrDefault(it, emptyList()) + pair.second }) }
