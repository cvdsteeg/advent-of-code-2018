package adventofcode

import java.io.File

class Day6(private val distanceFromAll: Int = 10000, private val stringCoordinates: List<String> = File("src/main/resources/day6.txt").readLines()) {
    companion object {
        private fun getCoordinates(stringCoordinates: List<String>): List<XY> = stringCoordinates
                .map { coordinateString -> coordinateString.split(Regex(",\\h")).filterNot { it.isEmpty() }.map { it.toInt() } }
                .map { XY(it[0], it[1]) }
                .sortedWith(compareBy({ it.x }, { it.y }))
    }

    private val coordinates = getCoordinates(stringCoordinates)

    private val min: XY = XY.fromPair(coordinates.map { it.x }.min()!! to coordinates.map { it.y }.min()!!)

    private val max: XY = XY.fromPair(coordinates.map { it.x }.max()!! to coordinates.map { it.y }.max()!!)

    fun assignment1(): Int = calculateClosestPoints(coordinates, generateGrid(min, max)).toList()
            .zip(calculateClosestPoints(coordinates, generateGrid(min - 1, max + 1)).toList())
            .filter { it.first.second.size == it.second.second.size }
            .map { it.first.second.size }
            .max()!!

    fun assignment2(): Int = gridToCoordinateManhattanDistance(generateGrid(min, max), coordinates)
            .map { it.value.sumBy { it.second } }
            .filter { it < distanceFromAll }
            .size

    private fun calculateClosestPoints(coordinates: List<XY>, grid: List<XY>): Map<XY, List<XY>> = gridToCoordinateManhattanDistance(grid, coordinates)
            .mapValues { gridToCoordinate -> gridToCoordinate.value.filter { it.second == gridToCoordinate.value.minBy { it.second }!!.second } }
            .filterValues { it.size == 1 }
            .mapValues { it.value.first() }
            .toList()
            .mapReverse { it.first }

    private fun gridToCoordinateManhattanDistance(grid: List<XY>, coordinates: List<XY>) =
            grid.map { gridPoint -> gridPoint to coordinates.map { it to gridPoint.calculateManhattanDistance(it) } }.toMap()
}

