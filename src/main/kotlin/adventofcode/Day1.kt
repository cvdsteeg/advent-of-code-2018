package adventofcode

import java.io.File

class Day1(private val freqs: List<Int> = File("src/main/resources/day1.txt").readLines().map { it.toInt() }) {
    fun assignment1(): Int {

        val firstFreqs = countFrequencies()

        return firstFreqs.last()
    }

    fun assignment2(): Int {

        val first = getFirstDuplicate()

        println(first)

        return first;
    }


    private tailrec fun getFirstDuplicate(total: List<Int> = countFrequencies(0)): Int {
        val second = countFrequencies(total.last())
        return second.intersect(total)
                .firstOrNull()?.let { it } ?: getFirstDuplicate(total + second)
    }

    private fun countFrequencies(initial: Int = 0): List<Int> {
        return freqs
                .fold(listOf(initial)) { a, b -> a + (b + a.last()) }
                .drop(1)
    }
}
