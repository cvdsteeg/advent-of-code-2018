package adventofcode

import java.io.File

class Day2(private val ids: List<String> = File("src/main/resources/day2.txt").readLines()) {

    fun assignment1(): Int {
        val groupBySize = ids
                .map { it.toCharArray() }
                .map { it.groupBy { it } }
                .map { letterGroup -> letterGroup.map { it.value.size } }
                .map { it.filter { it in listOf(2, 3) } }
                .map { it.distinct() }
                .flatten()
                .groupBy { it }

        return groupBySize.getOrDefault(2, listOf()).size * groupBySize.getOrDefault(3, listOf()).size
    }

    fun assignment2(): String {
        val idsArray = ids

        return sequenceOf(*ids.toTypedArray())
                .map { it to idsArray }
                .map { (k, v) -> getCommonsCharsMinusOne(k, v) }
                .filter { it != null }
                .map { it!! }
                .map { it.first.toList().zip(it.second.toList()).filter { (vi, ki) -> vi == ki }.map { it.first } }
                .map { it.joinToString("") }
                .first()
    }

    private fun getCommonsCharsMinusOne(id: String, ids: List<String>): Pair<String, String>? {
        return sequenceOf(*ids.toTypedArray())
                .map { value -> value to id }
                .filter { it.first.toList().zip(it.second.toList()).filter { (vi, ki) -> vi != ki }.size == 1 }
                .firstOrNull()
    }
}
