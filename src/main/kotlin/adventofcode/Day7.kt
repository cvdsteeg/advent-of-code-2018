package adventofcode

import java.io.File

class Day7(private val stepStrings: List<String> = File("src/main/resources/day7.txt").readLines()) {

    fun assignment1(): String = stepStrings
            .map { parseStep(it) }.toMapToList().mapValues { it.value.sorted() }.toSortedMap()
            .let { findStepOrder(it.keys.union(it.values.flatten().distinct()), it) }

    private fun findStepOrder(keys: Set<Char>, steps: Map<Char, List<Char>>, acc: String = ""): String = when {
        keys.isEmpty() -> acc
        else -> keys.first { steps.values.flatten().distinct().contains(it).not() }.let {
            findStepOrder(keys - it, steps - it, acc + it)
        }
    }

    fun assignment2(): String {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun parseStep(s: String) = s
            .replace(" must be finished before", "", true)
            .replace(" can begin.", "", true)
            .trim()
            .split(Regex("\\h?[sS]tep "))
            .let { Pair(it[1].first(), it[2].first()) }
}
