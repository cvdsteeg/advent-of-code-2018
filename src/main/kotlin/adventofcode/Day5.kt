package adventofcode

import java.io.File

class Day5(private val polymere: String = File("src/main/resources/day5.txt").readText()) {
    fun assignment1(): Int {
        return resolvePolymere(polymere).length
    }

    fun assignment2(): Int = polymere.toSet().map { it.toLowerCase() }
            .pmap { char -> resolvePolymere(polymere.filterNot { it.toLowerCase() == char }).length }
            .min()!!

    private fun resolvePolymere(polymere: String): String {
        return when {
            polymere.length < 2 -> polymere
            else -> polymere.fold("") { acc, s ->
                when {
                    acc.isEmpty() -> acc + s
                    acc.last().isLowerCase() && acc.last().toUpperCase() == s -> acc.dropLast(1)
                    acc.last().isUpperCase() && acc.last().toLowerCase() == s -> acc.dropLast(1)
                    else -> acc + s
                }
            }
        }
    }

}