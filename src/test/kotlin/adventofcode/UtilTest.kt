package adventofcode

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class UtilTest {
    @Test
    fun testManhattanDistance() {
        assertEquals(2, XY(0, 0).calculateManhattanDistance(XY(1, 1)))
        assertEquals(2, XY(0, 0).calculateManhattanDistance(XY(-1, -1)))
        assertEquals(4, XY(1, 1).calculateManhattanDistance(XY(-1, -1)))
        assertEquals(2, XY(0, 0).calculateManhattanDistance(XY(1, -1)))
    }

    @Test
    fun testGrid() {
        val coor = XY(5, 5)
        val grid = generateGrid(XY(0, 0), XY(10, 10))
        print(grid.map { coor.calculateManhattanDistance(it) }.joinToString(","))
    }
}