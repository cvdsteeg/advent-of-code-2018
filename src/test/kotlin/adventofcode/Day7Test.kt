package adventofcode

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day7Test {
    private val steps = listOf(
            "Step C must be finished before step A can begin.",
            "Step C must be finished before step F can begin.",
            "Step A must be finished before step B can begin.",
            "Step A must be finished before step D can begin.",
            "Step B must be finished before step E can begin.",
            "Step D must be finished before step E can begin.",
            "Step F must be finished before step E can begin."
    )

    @Test
    fun assignment1test() {
        Assertions.assertEquals("CABDFE", Day7(steps).assignment1())
    }

    @Test
    fun assignment2test() {
        Assertions.assertEquals("", Day7(steps).assignment2())
    }

    @Test
    fun assignment1() {
        Assertions.assertEquals("EFHLMTKQBWAPGIVXSZJRDUYONC", Day7().assignment1())
    }

    @Test
    fun assignment2() {
        Assertions.assertEquals("", Day7().assignment2())
    }
}