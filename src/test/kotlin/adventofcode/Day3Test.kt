package adventofcode

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day3Test {

    @Test
    fun assignment1test() {
        val test = listOf("#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2")
        val ass1 = Day3(test).assignment1()
        Assertions.assertEquals(4, ass1)
    }

    @Test
    fun assignment1() {
        val ass1 = Day3().assignment1()
        Assertions.assertEquals(118223, ass1)
    }

    @Test
    fun assignment2test() {
        val test = listOf("#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2")
        val ass1 = Day3(test).assignment2()
        Assertions.assertEquals(3, ass1)
    }

    @Test
    fun assignment2() {
        val ass1 = Day3().assignment2()
        Assertions.assertEquals(412, ass1)
    }
}