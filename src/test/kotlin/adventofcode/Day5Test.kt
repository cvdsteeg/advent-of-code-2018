package adventofcode

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day5Test {
    private val polymere = "dabAcCaCBAcCcaDA"

    @Test
    fun assignment1test() {
        Assertions.assertEquals(10, Day5(polymere).assignment1())
    }

    @Test
    fun assignment1() {
        Assertions.assertEquals(9462, Day5().assignment1())
    }

    @Test
    fun assignment2test() {
        Assertions.assertEquals(4, Day5(polymere).assignment2())
    }

    @Test
    fun assignment2() {
        Assertions.assertEquals(4952, Day5().assignment2())
    }
}