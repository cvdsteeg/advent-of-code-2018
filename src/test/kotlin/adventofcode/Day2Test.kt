package adventofcode

import adventofcode.Day2
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day2Test {
    @Test
    fun assignment1test() {
        val ids = listOf("abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab")

        Assertions.assertEquals(12, Day2(ids).assignment1())
    }

    @Test
    fun assignment1() {

        Assertions.assertEquals(5434, Day2().assignment1())
    }

    @Test
    fun assignment2test() {
        val ids = listOf("abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz")

        Assertions.assertEquals("fgij", Day2(ids).assignment2())
    }

    @Test
    fun assignment2() {
        Assertions.assertEquals("agimdjvlhedpsyoqfzuknpjwt", Day2().assignment2())
    }
}
