package adventofcode

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

internal class Day6Test {
    private val coordinates = listOf("1, 1", "1, 6", "8, 3", "3, 4", "5, 5", "8, 9")

    @Test
    fun assignment1test() {
        Assertions.assertEquals(17, Day6(32, coordinates).assignment1())
    }

    @Test
    fun assignment2test() {
        Assertions.assertEquals(16, Day6(32, coordinates).assignment2())
    }

    @Test
    fun assignment1() {
        Assertions.assertEquals(3969, Day6().assignment1())
    }

    @Test
    fun assignment2() {
        Assertions.assertEquals(42123, Day6().assignment2())
    }
}